import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { MockedProvider } from '@apollo/react-testing';

const TestHooksComponent = ({ callback }: any): null => {
    callback();
    return null;
}

export const testHooksFunction = (callback: any): void => {
    mount(<TestHooksComponent callback={callback} />);
}

export const testHooksFunctionWithDBMocks = (callback: any, mocks: any[]): ReactWrapper => mount(
    <MockedProvider mocks={mocks} addTypename={false}>
        <TestHooksComponent callback={callback} />
    </MockedProvider>
)