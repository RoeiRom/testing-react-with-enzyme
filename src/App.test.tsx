import React from 'react';
import { mount } from 'enzyme';
import { MockedProvider, wait } from '@apollo/react-testing';
import { act } from 'react-dom/test-utils';
import { ALL_ENTITIES } from './queries/firstQuery/Query';
import App from './App';
import { CircularProgress, Typography } from '@material-ui/core';

const dbMocksPass = [
    {
        request: {
            query: ALL_ENTITIES
        },
        result: {
            data: {
                allEntities: {
                    nodes: [
                        {
                            id: 1, 
                            title: 'mock1'
                        }
                    ]
                }
            }
        }
    }
]

const dbMocksFail = [
    {
        request: {
            query: ALL_ENTITIES
        },
        error: new Error('testing error')
    }
]

test('loading spinner should be displyed while fetching the results', () => {
    const wrapper = mount(
        <MockedProvider mocks={dbMocksPass} addTypename={false}>
            <App />
        </MockedProvider>
    );
    act(() => {
        wrapper.update();
        expect(wrapper.contains(<CircularProgress />)).toBeTruthy();
    })
})

test('hide the loading spinner when it is not loading', async () => {
    const wrapper = mount(
        <MockedProvider mocks={dbMocksPass} addTypename={false}>
            <App />
        </MockedProvider>
    );
    await act(async () => {
        await wait(0);
        wrapper.update();
    })
    expect(wrapper.contains(<CircularProgress />)).toBeFalsy();
})

test('should display error message when encountering an error', async () => {
    const wrapper = mount(
        <MockedProvider mocks={dbMocksFail} addTypename={false}>
            <App />
        </MockedProvider>
    );
    await act(async () => {
        await wait(0);
        wrapper.update();
    });
    expect(wrapper.contains(<span>There was an error!</span>)).toBeTruthy();
})

test('should display the data in the correct structure', async () => {
    const mappingFuction = (entity: { id: number, title: string }) => (
        <table key={entity.id}>
            <tbody>
              <tr>
                <td><Typography>Id</Typography></td>
                <td>{entity.id}</td>
              </tr>
              <tr>
                <td><Typography>Title</Typography></td>
                <td>{entity.title}</td>
              </tr>
            </tbody>
        </table>
    );
    
    const wrapper = mount(
        <MockedProvider mocks={dbMocksPass} addTypename={false}>
            <App />
        </MockedProvider>
    );

    await act(async () => {
        await wait(0);
        wrapper.update();
    });

    expect(wrapper.containsAllMatchingElements(dbMocksPass.map(mock => mock.result)[0].
                                                               data.allEntities.nodes.map(mappingFuction)))
                                                               .toBeTruthy();
})