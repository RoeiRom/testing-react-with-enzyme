import { gql } from 'apollo-boost';

export const ALL_ENTITIES = gql`
query Entities {
    allEntities {
      nodes {
        id
        title
      }
    }
  }
  
`;

export type ReturnType = {
  allEntities : {
    nodes: [{
      id: number,
      title: string
    }]
  }
}