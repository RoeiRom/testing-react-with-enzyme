import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

// @ts-ignore
window.HTMLCanvasElement.prototype.getContext = () => {};
window.URL.createObjectURL = jest.fn();