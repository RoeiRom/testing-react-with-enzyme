import React from 'react';
import './App.css';
import { ALL_ENTITIES, ReturnType } from './queries/firstQuery/Query';
import { useQuery } from 'react-apollo';
import { CircularProgress, Typography } from '@material-ui/core';

function App() {
  
  const {loading, data, error} = useQuery<ReturnType>(ALL_ENTITIES, {
    fetchPolicy: 'network-only'
  });
  
  return (
    <>
    {
      loading ?
        <CircularProgress />
        :
        error ?
          <span>There was an error!</span>
          :
          data &&
            data.allEntities.nodes.map(entity => (
              <table key={entity.id}>
                <tbody>
                  <tr>
                    <td><Typography>Id</Typography></td>
                    <td>{entity.id}</td>
                  </tr>
                  <tr>
                    <td><Typography>Title</Typography></td>
                    <td>{entity.title}</td>
                  </tr>
                </tbody>
              </table>
            ))
    }
    </>
  );
}

export default App;
